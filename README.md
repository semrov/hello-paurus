# Angular and TypeScript Hello World Project

The Hello World project is an Angular 2+ starter project that has the npm modules, 
configuration, scripts and folders and routing in place to make getting started 
with an Angular project easy!

## Running the Application

1. Install the Angular CLI

    `npm install -g @angular/cli`

1. Run `npm install`

1. Run `ng serve -o`