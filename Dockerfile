FROM centos:latest
RUN dnf -y update

RUN dnf -y module enable nodejs:14
RUN dnf -y install nodejs-devel
RUN npm install -g @angular/cli

RUN mkdir -p /store/apps/hello-paurus
COPY . /store/apps/hello-paurus

WORKDIR /store/apps/hello-paurus
RUN npm install

EXPOSE 4200
CMD ["ng", "serve", "--host", "0.0.0.0"]

LABEL maintainer="Jure Šemrov"
